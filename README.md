# WeTransfer spinner

## Demo

![Spinner demo](spinner-demo2.gif)

## How to run

*Built with Node.js v10.15.3 and npm v6.4.1.*

To check the live demo, please visit: [https://spinner-demo.netlify.com/](https://spinner-demo.netlify.com/)

### Running locally

1. In your terminal, run `git clone https://charliege@bitbucket.org/charliege/spinner.git` to clone the repository.
2. Navigate to the folder where you cloned the repo and install the npm packages by running `npm install`.
3. Run `npm start` to start the app and visit `http://localhost:8000`

### Tests

```
npm test
```

### Build

```
npm run build
```

## Assumptions

### Assumption #1
I worked on this test with the assumption that there would be no real upload feature.
As a result, the transfer information is hardcoded and static, except for the main transfer status that changes when clicking on the start/stop button.

Also, the progress of the spinner is hardcoded to last 5s and does not reflect how it would work if it was correlated to the progress of a file transfer.


### Assumption #2

The goal of this exercise was to build a reusable spinner component. I built it in a way that makes it reusable across this codebase if multiple spinners needed to be created, however, I did not make it a separate npm package to be used in any other external application.


## Tech stack

Front-end:

* HTML
* CSS
* JavaScript

Testing: 

* Jest

Tooling: 

* Babel
* Webpack

## Decisions

To build this, I decided not to use any framework and go with vanilla JavaScript.

I use React & Redux on a daily basis and wanted to try and go back to basics. 

I also thought using React.js would be overengineering considering the size of the code test, for no real benefit as I could achieve the same result with vanilla JavaScript.

Apart from this, I used a simple entry file in HTML and vanilla CSS.

For tooling, I used Babel to transpile my code from ES6 to ES5, and Webpack to create a minified bundle and allow me to organise my CSS as separate modules.

## Architecture

```markdown
- index.html
- index.js
- components/
    - Button/
    - Spinner/
    - TransferInformation/
    - Widget/
- dist/
- utils/
```

## How to reuse the spinner component

To reuse the spinner inside other components, start by importing it like this:

```javascript
import Spinner from '<path>/components/Spinner/index.js';
```

Then, instantiate a new Spinner object and call the `render` method to create and append it to the DOM:

```javascript
const spinner = new Spinner();
spinner.render();
```

When creating a new `Spinner` object, a parent element can be specified as a parameter to indicate where the element should be added on the page. If no element is passed, the default will be `document.body`.

## Methods available

* `render`: Creates and renders the element.
* `getSpinner`: returns the Spinner element.
* `getSpinnerText`: returns the `<text>` element that's inside the Spinner.
* `updateSpinner`: updates the spinner animation.
* `resetSpinner`: resets the spinner animation.
* `pauseSpinner`: pauses the spinner animation.
* `updatePercentage`: updates the percentage inside the spinner.

## Improvements

### CSS

In this exercise, I did not write much CSS but, in a bigger application, I would probably switched to using Sass or CSS-in-JS, depending on the size of the stylesheets.

### JavaScript

Building this app in vanilla JavaScript worked ok as there is not too many features but, if it were to be extended, I would probably switch to using a framework for better state management.

### Testing

I did not start with a "test first" approach as I wasn't sure how to organise my code when I first started.
If I had to add more features now, I could probably write the tests before the implementation as I have a better idea of how my code is organised. 

## Final thoughts

In a production application, I would probably make the following changes:

* Would like to add a type system (Flow/TypeScript)
* Would switch to a framework with a state management library to make it easier to maintain as the app grows.
* Would add tooling such as ESLint for coding standards, axe for accessibility checks, Webpack, and more.    


---

Thanks for reading!

