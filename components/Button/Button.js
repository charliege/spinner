import "./button.css";
import {pubSub} from '../../utils/pubsub.js';

export default class Button {
    constructor(text, containerElement){
        this.text = text;
        this.containerElement = containerElement || document.body;
        this.element;

        pubSub.subscribe("transferringStateUpdated", data => {
            this.updateText(data.isTransferring);
        });
    }

    publishEvent() {      
        return pubSub.publish("buttonClicked", {});
    }

    createButton(){
        const button = document.createElement('button');
        button.innerHTML = this.text;
        button.onclick = () => this.publishEvent();
        this.element = button;
        return button;
    }

    getButton(){
        return this.element;
    }

    updateText(state){
        return this.getButton().innerHTML = state ? 'Stop' : 'Resume';
    }

    reset(){
        return this.getButton().innerHTML = 'Start';
    }

    render(){
        const button = this.createButton();
        this.containerElement.appendChild(button);
    }
}