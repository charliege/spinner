import "./buttonContainer.css";

export default class ButtonContainer {
    constructor(buttons, containerElement){
        this.buttons = buttons;
        this.containerElement = containerElement || document.body;
    }

    createButtonContainer(){
        const buttonContainer = document.createElement('section');
        buttonContainer.classList.add('buttons-container');

        this.buttons.map(button => {
            const buttonElement = button.createButton();
            return buttonContainer.appendChild(buttonElement);
        });

        return buttonContainer;
    }

    render(){
        const buttonContainer = this.createButtonContainer();
        this.containerElement.appendChild(buttonContainer);
    }
}