import Button from './Button.js';
import {pubSub} from '../../utils/pubsub.js';

jest.mock('../../utils/pubsub.js');

describe("Button component", function() {  
    let buttonElement, buttonComponent;
    beforeEach(() => {
        buttonComponent = new Button('Start', document.body);
        buttonElement = buttonComponent.createButton();
    });

    it("should be visible", function() {
        expect(buttonElement).toBeVisible();
    });

    it("should contain the correct text when initialised", function() {
        expect(buttonElement.innerHTML).toBe("Start");
    });

    it("should publish an event on click", function() {
        buttonElement.click();
        expect(pubSub.publish).toHaveBeenCalledTimes(1);
        expect(pubSub.publish).toHaveBeenCalledWith('buttonClicked', {});
    });

    it("should update the button text", function() {
        buttonComponent.updateText(true);
        expect(buttonElement.innerHTML).toBe("Stop");

        buttonComponent.updateText(false);
        expect(buttonElement.innerHTML).toBe("Resume");
    });

    it("should reset button text when reset is called", function(){
        buttonComponent.updateText(true);
        expect(buttonElement.innerHTML).toBe("Stop");
        buttonComponent.reset();
        expect(buttonElement.innerHTML).toBe("Start");
    })
});