import InformationText from '.';

describe("InformationText component", function() {  
    let infoTextComponent;
    beforeEach(() => {
        infoTextComponent = new InformationText();
        infoTextComponent.render();
    });

    it('should render correctly', function(){
        expect(infoTextComponent.getInfoContainer()).toMatchSnapshot()
    });

    it("should have the correct initial state for transferring", function() {
        expect(infoTextComponent.transferringState).toBe('Transfer files');

        const transferringTitle = infoTextComponent.getTransferTitle();
        expect(transferringTitle.innerHTML).toBe('Transfer files');
    });

    it("should update the text displayed when updateTransferringState is called", function() {
        infoTextComponent.updateTransferringState('transferring');
        const transferringTitle = infoTextComponent.getTransferTitle();
        expect(transferringTitle.innerHTML).toBe("Transferring...");

        infoTextComponent.updateTransferringState('paused');
        expect(transferringTitle.innerHTML).toBe("Transfer paused");
    });
});