import {pubSub} from '../../utils/pubsub.js';
import "./transferInformation.css";

const transferringStates = {
    'default': 'Transfer files',
    'transferring': "Transferring...",
    'paused': "Transfer paused",
    'finished': "Transfer finished"
}

export default class TransferInformation {
    constructor(container){
        this.transferringState = transferringStates.default;
        this.transferTitle = "Transfer files";
        this.titleElement;
        this.container = container || document.body;
        this.transferComplete = false;
        this.element;

        pubSub.subscribe("transferringStateUpdated", data => {
            this.updateTransferringState(data.transferState);
        });
    }

    createTransferTitle(){
        this.titleElement = document.createElement('h4');
        this.titleElement.innerHTML = this.transferTitle; 
        this.titleElement.classList.add('transferring-title'); 
        return this.titleElement;
    }

    getTransferTitle(){
        return this.titleElement;
    }

    createTransferContent(){
        const transferDetails = document.createElement('p');
        transferDetails.innerHTML = `
            <p>Sending <a href="">11 files to 4 recipients</a></p>
            <p>648MB of 1.8GB uploaded</p>    
            <p>33 minutes remaining</p>    
        `; 
        transferDetails.classList.add('transfer-details'); 
        return transferDetails;
    }

    getInfoContainer(){
        return this.element;
    }

    createInfoTextContainer(){
        const container = document.createElement('section');
        container.classList.add('container-information');
        const title = this.createTransferTitle();
        const transferDetails = this.createTransferContent();
        container.appendChild(title);
        container.appendChild(transferDetails);
        this.element = container;
        this.container.appendChild(container);
    }

    updateTransferringState(state){
        this.getTransferTitle().innerHTML = transferringStates[state];
        this.transferringState = transferringStates[state];            
    }

    render(){
        this.createInfoTextContainer();
    }
}