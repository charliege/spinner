import Widget from '.';
import 'mutationobserver-shim';

describe("Widget component", function() {  
    let widgetComponent;
    beforeEach(() => {
        widgetComponent = new Widget(document.body);
        widgetComponent.render();
    });

    it('should render correctly', function(){
        expect(widgetComponent.getContainer()).toMatchSnapshot()
    });
});