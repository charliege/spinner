import Button from '../Button/Button.js';
import ButtonContainer from '../Button/ButtonContainer.js';
import Spinner from '../Spinner/index.js';
import TransferInformation from '../TransferInformation/index.js';
import "./widget.css";
import {pubSub} from '../../utils/pubsub.js';

let infoText, spinner, startButton;

export default class Widget {
    constructor(container){
        this.transferring = false;
        this.transferFinished = false;
        this.container = container;

        pubSub.subscribe("buttonClicked", data => {
            this.publishEvent();

            if(this.transferFinished){
                this.resetSpinner()
            }
        });
    }

    publishEvent() {
        this.transferring = !this.transferring;
        this.transferring ? spinner.updateSpinner() : spinner.pauseSpinner();
        const transferState = this.transferring ? 'transferring' : 'paused';

        const data = {isTransferring: this.transferring, transferState};

        pubSub.publish("transferringStateUpdated", data);
    }

    getSpinner(container){
        return new Spinner(container);
    }

    getInfoText(container){
        infoText = new TransferInformation(container);
        return infoText;
    }

    getButtons(container){
        startButton = new Button('Start', container);
        return [startButton];
    }

    resetSpinner(){
        // Solution from here: https://stackoverflow.com/questions/6268508/restart-animation-in-css3-any-better-way-than-removing-the-element
        
        spinner.getSpinner().style.webkitAnimation = 'none';
        setTimeout(function() {
            spinner.getSpinner().style.webkitAnimation = '';
        }, 10);
        this.transferFinished = false;
    }

    getButtonsContainer(container){
        const buttons = this.getButtons(container);
        const buttonSection = new ButtonContainer(buttons, container);
        return buttonSection;
    }

    reset(){
        startButton.reset();
        spinner.resetSpinner();
        this.transferring = false;
    }

    getContainer(){
        return document.getElementsByClassName('widget')[0];
    }

    renderContainer(){
        const widget = document.createElement('section');
        widget.classList.add('widget');
        this.container.appendChild(widget);

        spinner = this.getSpinner(widget);
        spinner.render();

        const infoText = this.getInfoText(widget);
        infoText.render();        
     
        const buttonsContainer = this.getButtonsContainer(widget);
        buttonsContainer.render();

        const spinnerElement = spinner.getSpinnerText();
        // Mutation Observer to detect when the upload is 'finished'.
        const observer = new MutationObserver(callback);
        observer.observe(spinnerElement, { childList: true });
        const self = this;
        function callback(mutationRecord, observer) {
            const spinnerValue = Array.from(mutationRecord[0].addedNodes)[0].data;
            if(spinnerValue === '100'){
                infoText.updateTransferringState('finished');
                self.reset();
                self.transferFinished = true;
            }
        }
    }

    render(){
        this.renderContainer();
    }
}