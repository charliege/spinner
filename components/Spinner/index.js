import "./spinner.css";

let updatePercentage;
export default class Spinner {
    constructor(container){
        this.coordinates = 61;
        this.radius = 58;
        this.circumference = this.radius * 3.14 * 2;
        this.strokeWidth = 6;
        this.spinning = false;
        this.currentTransferPercentage = 0;
        this.container = container || document.body;
    }

    getSpinner(){
        const circle = document.querySelector('.progress-ring__circle');
        return circle;
    }

    getSpinnerText(){
        const circle = document.querySelector('text');
        return circle;
    }

    updateSpinner(){
        const circle = this.getSpinner();
        const offset = this.circumference * this.circumference;

        circle.style.stroke = "rgb(55,149,255)";
        circle.style.strokeDashoffset = offset;  
        circle.classList.add('progress-ring__circle-running');  
        this.spinning = true;
    
        this.updatePercentage(this.spinning);
    }

    resetSpinner(){
        const circle = this.getSpinner();
        const offset = this.circumference * this.circumference;
        
        circle.style.strokeDashoffset = offset;  
        circle.classList.remove('progress-ring__circle-running');  
        this.spinning = false;
        this.currentTransferPercentage = 0;
        this.updatePercentage(this.spinning);
    }

    pauseSpinner(){
        const spinner = this.getSpinner();
        spinner.classList.replace('progress-ring__circle-running', 'progress-ring__circle-paused');
        this.spinning = false;

        this.updatePercentage(this.spinning);
    }

    updatePercentage(spinning){
        const text = document.getElementsByClassName('percentage')[0];
        var self = this;
        
        if(spinning){
            updatePercentage = setInterval(function(){
                if(self.currentTransferPercentage > 100){
                    return self.spinning = false
                }
                text.innerHTML = `${self.currentTransferPercentage++}<tspan class="percentage-sign">%</tspan>`;
            }, 50);
        } else {
            clearInterval(updatePercentage)
        }
    }

    render(){
        const spinner = document.createElement('section');
        spinner.classList.add('spinner-section');
        spinner.innerHTML = `
            <svg
                class="progress-ring"
                height=${this.coordinates * 2}
                width=${this.coordinates * 2}
            >
            <g>
                <circle
                    class="progress-ring__container"
                    stroke-width=${this.strokeWidth}
                    stroke="lightgrey"
                    fill="transparent"
                    r=${this.radius}
                    cx=${this.coordinates}
                    cy=${this.coordinates}
                />
                <circle
                    class="progress-ring__circle"
                    stroke-width="6"
                    stroke-dasharray=${this.circumference}
                    stroke-dashoffset=${this.circumference}
                    fill="transparent"
                    r=${this.radius}
                    cx=${this.coordinates}
                    cy=${this.coordinates}
                />
                <text x="60" y=${this.coordinates + 10} class="percentage" font-family="Verdana" text-anchor="middle" font-size="35" fill="black"></text>
            </g>
            </svg>
        `;
        this.container.appendChild(spinner);

        return spinner;
    }
}