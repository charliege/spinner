import Spinner from '.';

describe("Spinner component", function() {  
    let spinner;
    beforeEach(() => {
        spinner = new Spinner(document.body);
        spinner.render();
    });

    it('should render correctly', function(){
        expect(spinner.render()).toMatchSnapshot()
    });

    it("should have an initial spinning state of false", function() {
        expect(spinner.spinning).toBe(false);
    });

    it("should set the spinning state to true and add the correct class when updating", function() {
        spinner.updateSpinner();
        const spinnerElement = spinner.getSpinner();

        expect(spinner.spinning).toBe(true);
        expect(spinnerElement.classList).toContain('progress-ring__circle-running');
    });

    it("should change the class, percentage and state on pause", function() {
        spinner.pauseSpinner();
        const spinnerElement = spinner.getSpinner();

        expect(spinner.spinning).toBe(false);
        expect(spinnerElement.classList).toContain('progress-ring__circle-paused');
    });

    it("should update the percentage with the spinner style", function(){
        const updatePercentageFn = jest.spyOn(spinner, 'updatePercentage');
        spinner.updateSpinner();
        expect(spinner.spinning).toBe(true);
        expect(updatePercentageFn).toHaveBeenCalled();
        expect(updatePercentageFn).toHaveBeenCalledWith(spinner.spinning);
    })

    it("should clear the interval when the upload is finished", function(){
        jest.useFakeTimers();
        
        spinner.updatePercentage(true);
        spinner.currentTransferPercentage = 101;
        expect(spinner.spinning).toBe(false);
        spinner.updatePercentage(spinner.spinning);
        expect(clearInterval).toHaveBeenCalled();
    })
});