import Widget from './components/Widget/index.js';

const app = document.getElementsByTagName('main')[0];
const widgetContainer = new Widget(app);
widgetContainer.render();